<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
      html, body { height: 100%; margin: 0; padding: 0; }
      #map { height: 70%; }
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  </head>
  <body>

    <div id="map"></div>


	<form action="server.php" method="POST" id="form">
		<input type="text" id="username" name="username" required/>
		<input type="submit" value="Add User"/>
		<p id="coords">
			
		</p>
	</form>

	<button id="nearBy">
		Load NearBy
	</button>



    <script type="text/javascript">

		function initMap() {




		  var myLatLng = {lat: 28.6204760, lng: 77.3893230};

		  var map = new google.maps.Map(document.getElementById('map'), {
		    zoom: 20,
		    center: myLatLng
		  });

		  var marker = new google.maps.Marker({
		    position: myLatLng,
		    map: map,
		    draggable : true,
		    title: 'Hello World!'
		  });
      		


		  google.maps.event.addListener(marker, 'dragend', function(evt){
    document.getElementById('coords').innerHTML = '<p>Marker dropped: Current Lat: <label id="lat">' + evt.latLng.lat().toFixed(20)+'</label>' + ' Current Lng: <label id="long">' + evt.latLng.lng().toFixed(20) + '</label></p>';
});


		  //Bind the click button to get nearby data

		  $(document).on('click' , '#nearBy' , function(){

		  	var latitude = $('#lat').text();
    		
    		var longitude = $('#long').text();

		  	//nearbyData(map , latitude, longitude);

		  });

		}


    </script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBv_zaAzq9cGtg_U3_ezIEqJOmwGH-GyjM&callback=initMap">
    </script>

    <script>
    $(function(){

    	//Load the data from database in put it into the map

    	loadData = function(map){
    		$.ajax({
    			url : 'server.php',
    			type : 'POST',
    			data : { task : 'retrieve' }
    		}).success(function(data){
    			var _data = JSON.parse(data);
    			
    			for(var i =0; i< _data.length ; i++){
    				var _d = _data[i];

    				var user = _d[0];
    				var lat  = _d[1];
    				var lng  = _d[2];
    				var myLatLng = {lat : parseFloat(lat) , lng : parseFloat(lng)};
    				var marker = new google.maps.Marker({
		    		position: myLatLng,
		    		map: map,
		    		draggable : true,
		    		title: user
				  });

    				

    		}

    		}).fail(function (){
    			alert('Handle this error ');
    		});
    	};


    	nearbyData = function(map , latitude , longitude){
    		$.ajax({
    			url : 'server.php',
    			type : 'POST',
    			data : { task : 'nearby' , distance : 2, lat : latitude , lng : longitude }
    		}).success(function(data){
    			var _data = JSON.parse(data);
    			
    			for(var i =0; i< _data.length ; i++){
    				var _d = _data[i];

    				var user = _d[0];
    				var lat  = _d[1];
    				var lng  = _d[2];
    				var myLatLng = {lat : parseFloat(lat) , lng : parseFloat(lng)};
    				var marker = new google.maps.Marker({
		    		position: myLatLng,
		    		map: map,
		    		draggable : true,
		    		title: user
				  });

    				

    		}

    		}).fail(function (){
    			alert('Handle this error ');
    		});	
    	};


    	$(document).on('submit' , '#form' , function(e){
    		e.preventDefault();
    		var username = $('#username').val();
    		var latitude = $('#lat').text();
    		var longitude = $('#long').text();
    		$.ajax({
    			url : 'server.php',
    			data : {user : username , lat : latitude , lng : longitude , task : 'add'},
    			type : 'POST'
    		}).success(function(data){

    			console.log(data);

    		}).fail(function(){
    			alert('Handle this error ');
    		});

    	});
    });
    </script>
  </body>
</html>